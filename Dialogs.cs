using System;
using System.Collections.Generic;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

partial class MainWindow{
	class AddDlg:Dialog
	{
		protected MainWindow parent;
		[UI] protected Box _dataBox = null;
		[UI] protected Button _applyButton = null;
		public AddDlg (MainWindow pt)
			: this(pt,new Builder("MainWindow.glade")) {
			}

		private AddDlg(MainWindow pt,Builder builder) 
			: base(builder.GetRawOwnedObject("_baseDlg"))
		{
			parent=pt; 
			builder.Autoconnect(this);
			_applyButton.Clicked+=Aplly_clicked;
			((_applyButton.Parent as ButtonBox).Children[1] 
			 as Button).Clicked+=delegate{	Destroy();};
			ShowAll();
		}
		protected List<object> GetDlgData(){
			var data=new List<object>();
			foreach(Box c in _dataBox)
				if (c.Children[1] is Entry)
					data.Add( ((Entry)(c.Children[1])).Text );
				else foreach(CheckButton cb in c)
					data.Add(cb.Active);
			return data;
		}
		protected virtual void Aplly_clicked(object sender, EventArgs a){
			var data=GetDlgData();
			data.Insert(0,Guid.NewGuid().ToString().Substring(0,5));
			parent._store.AppendValues(data.ToArray());
			parent.Modifications.Add(data[0] as string,new List<string>());
			Destroy();
		}

	}
	class EditStud:AddDlg
	{
		TreeIter iter;
		List<object> oldData=new List<object>();
		public EditStud (MainWindow pt)
			:base (pt){
				pt._store.GetIter(out iter,
						pt._treeView.Selection.GetSelectedRows()[0]);
				int i=1;
				foreach(Box c in _dataBox)
					if (c.Children[1] is Entry){
						var t=pt._store.GetValue(iter,i++);
						((Entry)(c.Children[1])).Text=t as string;
						oldData.Add(t);
					}
					else foreach(CheckButton cb in c){
						var t=pt._store.GetValue(iter,i++);
						cb.Active=(bool)t;
						oldData.Add(t);
					}
			}
		protected override void Aplly_clicked(object sender, EventArgs a){
			var newData=GetDlgData();
			var id=parent._store.GetValue(iter,0) as string;
			var History=parent.Modifications[id];
			for(int i=0;i<newData.Count;i++)
				if (!newData[i].Equals(oldData[i])){
					string partName=parent._treeView.Columns[i+1].Title;
					if (newData[i] is string)
						History.Add($"{partName}: {oldData[i]} -> {newData[i]}");
					if (newData[i] is bool){
						string update=(bool)oldData[i]? "вкрали":"поставили";
						History.Add($"{update} {partName}");
					}
				}
			parent.Modifications[id]=History;
			newData.Insert(0,id);
			parent._store.SetValues(iter,newData.ToArray());
			Destroy();
		}
	}

	class RowShowerDlg:Dialog{
		private MainWindow parent;
		private Box infoBox;
		public RowShowerDlg(MainWindow pt):base(){
			parent=pt;
			Title="Яку інформацію показувати";
			AddButton("gtk-apply",ResponseType.Apply);
			infoBox=(Box)Children[0];
			foreach(var c in parent._treeView.Columns){
				var CB=new CheckButton(c.Title);
				CB.Active=c.Visible;
				infoBox.Add(CB);
			}
			Response+=ResopnseHandler;
			ShowAll();
		}
		private void ResopnseHandler(object sender, EventArgs a){
			if(((ResponseArgs)a).ResponseId==ResponseType.Apply )
				for(int i=0;i<parent._treeView.Columns.Length;i++)
					parent._treeView.Columns[i].Visible=
						((CheckButton)(infoBox.Children[i])).Active;
			Destroy();
		}
	}
}
