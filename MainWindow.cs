using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Serialization;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

partial class MainWindow : Window
{
	[UI] private ListStore _store = null;
	[UI] private Box _controlBox=null,_controlBox1 = null;
	[UI] private TreeView _treeView= null;
	private Dictionary<string,List<string>> Modifications;

	public MainWindow() : this(new Builder("MainWindow.glade")) { }

	private MainWindow(Builder builder) : 
		base(builder.GetRawOwnedObject("MainWindow"))
	{
		builder.Autoconnect(this);
		Modifications=new Dictionary<string, List<string>>();

		foreach(var c in _treeView.Columns)
			c.Clicked+=Column_Clicked;
		DeleteEvent += Window_DeleteEvent;
		( _controlBox.Children[0] as Button).Clicked+=AddB_Clicked;
		( _controlBox.Children[1] as Button).Clicked+=CngB_Clicked;
		( _controlBox.Children[2] as Button).Clicked+=DelB_Clicked;
		( _controlBox.Children[3] as Button).Clicked+=HistB_Clicked;
		( _controlBox1.Children[0] as Button).Clicked+=SerB_Clicked;
		( _controlBox1.Children[1] as Button).Clicked+=DsrB_Clicked;
		( _controlBox1.Children[2] as Button).Clicked+=InfoB_Clicked;
	}
	private void Column_Clicked(object sender, EventArgs a)
	{
		TreeViewColumn column=sender as TreeViewColumn;
		int i=
			Array.FindIndex(_treeView.Columns, e => e == column);
		if(column.SortOrder==SortType.Ascending)
			column.SortOrder=SortType.Descending;
		else 
			column.SortOrder=SortType.Ascending;

		_store.SetSortColumnId(i,column.SortOrder);
	}

	private void DelB_Clicked(object sender, EventArgs a)
	{
		if(_treeView.Selection.CountSelectedRows()<=0)
			return;
		TreeIter iter;
		_store.GetIter(out iter,
				_treeView.Selection.GetSelectedRows()[0]);
		_store.Remove(ref iter);
	}
	private void HistB_Clicked(object sender, EventArgs a)
	{
		if(_treeView.Selection.CountSelectedRows()<=0)
			return;
		TreeIter iter;
		_store.GetIter(out iter,
				_treeView.Selection.GetSelectedRows()[0]);
		var id=_store.GetValue(iter,0) as string;
		var str=String.Join("\n",Modifications[id].ToArray());
		if(str.Length==0) str="Поки немає змін";
		var dlg=new MessageDialog(this,DialogFlags.Modal,MessageType.Info,
				ButtonsType.Ok,str);
		dlg.Run();
		dlg.Destroy();

	}
	private void SerB_Clicked(object sender, EventArgs a)
	{

		var fdlf=new FileChooserDialog("Зберегти як",this,
				FileChooserAction.Save,"Відміна",ResponseType.Cancel,
				"Серіалізувати",ResponseType.Ok);
		var ff=new FileFilter(); ff.Name="Xml файли"; ff.AddPattern("*.xml");
		fdlf.AddFilter(ff);

		if(ResponseType.Ok==(ResponseType)fdlf.Run()){
			string path=fdlf.File.Path;
			if (!path.EndsWith(".xml")) path+=".xml";
			var file = new FileStream(path,FileMode.Create);

			var data=new List<object[]>();
			foreach(object[] o in _store){
				var d=o.ToList();
				foreach(var s in Modifications[o[0] as string])
					d.Add(s);
				data.Add(d.ToArray());
			}
			( new XmlSerializer(typeof(List<object[]>)) ).
				Serialize(file,data);
		}
		fdlf.Destroy();
	}
	private void DsrB_Clicked(object sender, EventArgs a)
	{
		var fdlf=new FileChooserDialog("Відкрити",this,FileChooserAction.Open,
				"Відміна",ResponseType.Cancel,
				"Десеріалізувати",ResponseType.Ok);
		var ff=new FileFilter(); ff.Name="Xml файли"; ff.AddPattern("*.xml");
		fdlf.AddFilter(ff);

		if(ResponseType.Ok==(ResponseType)fdlf.Run()){
			var file = new FileStream(fdlf.File.Path,FileMode.Open);

			XmlSerializer format=new XmlSerializer(typeof(List<object[]>));
			var Data=(List<object[]>)(format.Deserialize(file)
					?? throw new Exception("null value converting"));

			int n=_treeView.Columns.Length;
			_store.Clear();
			Modifications.Clear();
			foreach(object[] i in Data){
				_store.AppendValues(i.Take(n).ToArray());
				var key=i[0] as string;
				Modifications[key]=i.Skip(n).OfType<string>().ToList();
			}
		}
		fdlf.Destroy();
	}
	private void AddB_Clicked(object sender, EventArgs a)
	{
		(new AddDlg(this)).Run();
	}
	private void CngB_Clicked(object sender, EventArgs a)
	{
		if(_treeView.Selection.CountSelectedRows()>0)
			(new EditStud(this)).Run();
	}
	private void InfoB_Clicked(object sender, EventArgs a)
	{
		(new RowShowerDlg(this)).Run();
	}

	private void Window_DeleteEvent(object sender, DeleteEventArgs a)
	{
		Application.Quit();
	}

}
